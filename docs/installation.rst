.. highlight:: shell

============
Installation
============


Stable release
--------------

To install taurus_pyqtgraph, run this command in your terminal:

.. code-block:: console

    $ pip install taurus_pyqtgraph

This is the preferred method to install taurus_pyqtgraph, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


